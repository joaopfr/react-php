const path = require('path');

module.exports = env => {
    return {
        entry: {
            main: path.resolve(__dirname, 'frontend-src/js/index.js')
        },
        output: {
            path: path.resolve(__dirname, 'public/js'),
            filename: 'main.js'
        },
        devtool: "#eval-source-map",
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader'
                    }
                }
            ]
        }
    };
};