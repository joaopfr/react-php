import {applyMiddleware, createStore, compose} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import todo from './reducers/index';
import createLogger from 'redux-logger';
import thunk from "redux-thunk";

// const thunk = store => next => action =>
//     typeof action === 'function' ? action(store.dispatch, store.getState) : next(action);

const configureStore = () => {
    const middlewares = [thunk];
    if (process.env.NODE_ENV !== 'production') {
       // middlewares.push(createLogger());
    }

    const enhancer = compose(
        applyMiddleware(...middlewares),
        composeWithDevTools()
    );

    return createStore(todo, enhancer);
};

export default configureStore;