import * as api from "../api";
import * as fromReducers from "../reducers";
import * as fromActionTypes from './actionTypes';

export const addTodoSuccess = todo =>({
    type: fromActionTypes.ADD_TODO_SUCCESS_ACTION_TYPE(),
    todo
});

export const addTodoFailure = message =>({
    type: fromActionTypes.ADD_TODO_FAILURE_ACTION_TYPE(),
    message
});

export const addTodo = text => dispatch => {
    return api.addTodo(text).then(
        response => dispatch(addTodoSuccess(response)),
        error => dispatch(addTodoFailure(error.message || 'Failed to add todo'))
    );
};

export const toggleTodoSuccess = todo => ({
    type: fromActionTypes.TOGGLE_TODO_SUCCESS_ACTION_TYPE(),
    todo
});

export const toggleTodo = id => dispatch => {
    return api.toggleTodo(id).then(
        response => dispatch(toggleTodoSuccess(response))
    );
};

const fetchTodosSuccess = (filter, todos) => ({
    type: fromActionTypes.FETCH_TODOS_SUCCESS_ACTION_TYPE(),
    todos,
    filter
});

const fetchTodosFailure = (filter, message) => ({
    type: fromActionTypes.FETCH_TODOS_FAILURE_ACTION_TYPE(),
    filter,
    message
});

const fetchTodosRequest = filter => ({
    type: fromActionTypes.FETCH_TODOS_REQUEST_ACTION_TYPE(),
    filter
});

export const fetchTodos = filter => (dispatch, getState) => {
    if (fromReducers.getIsFetching(getState(), filter)) {
        return Promise.resolve();
    }
    dispatch(fetchTodosRequest(filter));

    return api.fetchTodos(filter).then(
        response => dispatch(fetchTodosSuccess(filter, response)),
        error => dispatch(fetchTodosFailure(
            filter, error.message || 'Something went wrong.'
        ))
    );
};

