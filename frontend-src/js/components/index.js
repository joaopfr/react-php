import {Provider} from "react-redux";
import React from "react";
import {BrowserRouter} from 'react-router-dom';
import RouteSwitch from "./RouteSwitch";

const Root = ({store}) => (
    <Provider store={store}>
        <BrowserRouter>
            <RouteSwitch/>
        </BrowserRouter>
    </Provider>
);

export default Root;