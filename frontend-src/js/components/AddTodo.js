import {connect} from "react-redux";
import React from "react";
import {addTodo} from "../actions/actions";

let AddTodoComponent = ({dispatch}) => {
    let input;
    return (
        <div>
            <input type="text" ref={node => {input = node}}/>
            <button onClick={() => {
                dispatch(addTodo(input.value));
                input.value = '';
            }}>
                Add Todo
            </button>
        </div>
    );
};
export const AddTodo = connect()(AddTodoComponent);