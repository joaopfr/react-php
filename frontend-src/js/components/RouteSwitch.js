import {Switch, Route} from 'react-router-dom';
import TodoApp from "./TodoApp";
import React from "react";

const RouteSwitch = () => (
    <Switch>
        <Route path="/:filter?" component={TodoApp}/>
    </Switch>
);

export default RouteSwitch;