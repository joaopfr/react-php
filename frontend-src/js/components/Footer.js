import {FilterLink} from "./FilterLink";
import React from "react";

const Footer = () => (
    <div>
        <FilterLink  filter="all">All</FilterLink> {' '}
        <FilterLink  filter="completed">Completed</FilterLink> {' '}
        <FilterLink  filter="active">Not Completed</FilterLink>
    </div>
);

export default Footer;