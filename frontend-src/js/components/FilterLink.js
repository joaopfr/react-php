import {Link} from 'react-router-dom';
import React from "react";

export const FilterLink = ({filter, children}) => {
    const path = `/${filter}`;
    return (
        <Link to={path}>
            {children}
        </Link>
    );
};