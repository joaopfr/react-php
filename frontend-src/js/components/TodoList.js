import {connect} from "react-redux";
import React from "react";
import {withRouter} from "react-router-dom";
import * as actions from '../actions/actions';
import * as fromReducers from "../reducers/index";
import FetchError from "./FetchError";

export const Todo = ({onClick, completed, text}) => {
    return (
        <li onClick={onClick} style={{textDecoration: (completed ? 'line-through' : 'none')}}>
            {text}
        </li>
    );
};

export const TodoList = ({todos, onTodoClick}) => {
    return (
        <ul>
            {todos.map(todo => {
                return (
                    <Todo key={todo.id} onClick={() => onTodoClick(todo.id)} {...todo}/>
                );
            })}
        </ul>
    );
};

export class _VisibleTodoList extends React.Component {
    componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.filter !== this.props.filter) {
            this.fetchData();
        }
    }

    fetchData() {
        const {filter, fetchTodos} = this.props;
        fetchTodos(filter).then(() => console.log("done"));
    }

    render() {
        const {isFetching, errorMessage, toggleTodo, todos} = this.props;
        if (isFetching && !todos.length) {
            return <p>Loading...</p>
        }
        if (errorMessage && !todos.length) {
            return (
                <FetchError message={errorMessage} onRetry={() => this.fetchData()}/>
            );
        }
        return <TodoList onTodoClick={toggleTodo} todos={todos}/>;
    }
}

const mapStateToProps = (state, {match}) => {
    const filter = match.params.filter || 'all';
    return {
        todos: fromReducers.getVisibleTodos(state, filter),
        isFetching: fromReducers.getIsFetching(state, filter),
        errorMessage: fromReducers.getErrorMessage(state, filter),
        filter
    };
};

// const mapDispatchToProps = dispatch => ({
//     onTodoClick(id) {
//         dispatch(toggleTodo(id));
//     }
// });

export const VisibleTodoList = withRouter(connect(
    mapStateToProps, actions
)(_VisibleTodoList));
