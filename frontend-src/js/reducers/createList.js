import * as fromActionTypes from "../actions/actionTypes";
import {combineReducers} from "redux";

const createList =  filter => {
    const handleToggleTodo = (state, action) => {
        const {id: toggledId, completed} = action.todo;
        const shouldRemove = filter === 'active' && completed || filter === 'completed' && !completed;
        return shouldRemove ? state.filter(id => id !== toggledId) : state;
    };

    const ids = (state = [], action) => {
        switch (action.type) {
            case fromActionTypes.FETCH_TODOS_SUCCESS_ACTION_TYPE():
                return action.filter === filter ? action.todos.map(todo => todo.id) :
                    state;
            case fromActionTypes.ADD_TODO_SUCCESS_ACTION_TYPE():
                return filter !== 'completed' ? [...state, action.todo.id] :
                    state;
            case fromActionTypes.TOGGLE_TODO_SUCCESS_ACTION_TYPE():
                return handleToggleTodo(state, action);
            default:
                return state;
        }
    };

    const isFetching = (state = false, action) => {
        if (action.filter !== filter) {
            return state;
        }
        switch (action.type) {
            case fromActionTypes.FETCH_TODOS_REQUEST_ACTION_TYPE():
                return true;
            case fromActionTypes.FETCH_TODOS_SUCCESS_ACTION_TYPE():
            case fromActionTypes.FETCH_TODOS_FAILURE_ACTION_TYPE():
                return false;
            default:
                return state;
        }
    };

    const errorMessage = (state = null, action) => {
        if (action.filter !== filter) {
            return state;
        }
        switch (action.type) {
            case fromActionTypes.FETCH_TODOS_FAILURE_ACTION_TYPE():
                return action.message;
            case fromActionTypes.FETCH_TODOS_REQUEST_ACTION_TYPE():
            case fromActionTypes.FETCH_TODOS_SUCCESS_ACTION_TYPE():
                return null;
            default:
                return state;
        }
    };

    return combineReducers({
        ids,
        isFetching,
        errorMessage
    });
};

export default createList;

export const getIds = state => state.ids;
export const getIsFetching = state => state.isFetching;
export const getErrorMessage = state => state.errorMessage;