import * as fromActionTypes from "../actions/actionTypes";

export const getTodo = (state, id) => state[id];

const byId = (state = {}, action) => {
    switch (action.type) {
        case fromActionTypes.FETCH_TODOS_SUCCESS_ACTION_TYPE():
            const nextState = {...state};
            action.todos.forEach(todo => nextState[todo.id] = todo);
            return nextState;
        case fromActionTypes.ADD_TODO_SUCCESS_ACTION_TYPE():
        case fromActionTypes.TOGGLE_TODO_SUCCESS_ACTION_TYPE():
            return {
                ...state,
                [action.todo.id]: action.todo
            };
        default:
            return state;
    }
};

export default byId;